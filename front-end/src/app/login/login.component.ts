import { Component, OnInit } from '@angular/core';
import {AuthService} from '../auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginUserData = {username: '', password: ''};
  error = {state: false, content: ''};
  constructor(private _auth: AuthService, private _router: Router ) { }

  ngOnInit(): void {
  }

  loginUser() {
    this._auth.loginUser(this.loginUserData)
      .subscribe(
        res => {
          this.error.state = false;
          if (res.status === 1) {
            localStorage.setItem('token', res.token);
            this._router.navigate(['/profile']);
          } else if (res.status === 0) {
            this.error.state = true;
            this.error.content = res.error;
          }
        },
        err => {
          this.error.state = true;
          this.error.content = err;
        }
      );
  }

}

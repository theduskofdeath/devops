import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';

@Injectable()
  export class AuthService {

    private base_url = 'http://localhost:8000/api/';
    constructor(private http: HttpClient, private _router: Router) {}

    registerUser(user) {
      return this.http.post<any>(this.base_url + 'register', user);
    }

    loginUser(user) {
      return this.http.post<any>(this.base_url + 'login', user);
    }

    loggedIn() {
      return !!localStorage.getItem('token');
    }

    getToken() {
      return localStorage.getItem('token');
    }

    logoutUser() {
      localStorage.removeItem('token');
      this._router.navigate(['/login'])
    }
  }

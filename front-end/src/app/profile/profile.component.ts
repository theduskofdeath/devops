import { Component, OnInit } from '@angular/core';
import {ProfileService} from '../profile.service';
import {HttpErrorResponse} from '@angular/common/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  text = '';
  constructor(private profileService: ProfileService, private router: Router) { }

  ngOnInit(): void {
    this.profileService.getUserProfile()
      .subscribe(
        res => {
          console.log(res.data);
          this.text = JSON.stringify(res.data);
        },
        error => {
          if (error instanceof HttpErrorResponse) {
            if (error.status === 401) {
              this.router.navigate(['/login']);
            }
          }
        }
      );
  }

}

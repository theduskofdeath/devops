import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  private base_url = 'http://localhost:8000/api/';
  constructor(private http: HttpClient) { }

  getUserProfile() {
    return this.http.get<any>(this.base_url + 'profile');
  }
}

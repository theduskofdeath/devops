let mysql = require('mysql')

let con = mysql.createConnection({
    host: "localhost",
    port: "3306",
    user: "root",
    password: "",
    database: "devops"
})

module.exports = con;

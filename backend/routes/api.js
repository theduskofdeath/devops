const express = require('express')
const jwt = require('jsonwebtoken')
const router = express.Router()
const con = require('../db')

function verifyToken(req, res, next) {
    if (!req.headers.authorization) {
        return res.status(401).send('Unauthorized request');
    }
    let token = req.headers.authorization.split(' ')[1]
    if (token === 'null') {
        return res.status(401).send('Unauthorized request');
    }
    let payload = null;
    try {
        payload = jwt.verify(token, 'secret');
        if (!payload)
            throw new Error('Unauthorized request');
    } catch (err) {
        return res.status(401).send('Unauthorized request');
    }
    req.userId = payload.subject;
    next();
}

router.get('/', (req, res) => {
    res.send('From API route')
})

router.get('/profile', verifyToken, (req, res) => {
    con.query('SELECT * FROM users WHERE username = ?', [req.userId], (err, result, fields) => {
        if (!err) {
            res.send({status: 1, data: result});
        } else {
            res.send({status: 0, error: err});
        }
    })
})

router.post('/login', async function (req, res, next) {
    let {username, password} = req.body;
    con.query('SELECT id FROM users WHERE username = ? AND password = ?', [username, password], (err, result, fields) => {
        if (err || result.length == 0) {
            res.send({status:0, error:"Credentials error"})
        } else {
            let payload = { subject: username};
            let token = jwt.sign(payload, 'secret');
            res.send({status: 1, data:req.body, token: token})
        }
    })
})

router.post('/register', async function (req, res, next) {
    try {
        let { username, email, password} = req.body;
        const checkUsername = 'SELECT username FROM users WHERE username = ?';
        con.query(checkUsername, [username], (err, result, fields) => {
            if (!err && result.length == 0) {
                const sql = 'INSERT INTO users (username, email, password) VALUES (?, ?, ?)';
                con.query(
                    sql, [username, email, password],
                    (err, result, fields) => {
                        if (err) {
                            res.send({status: 0, error: err});
                        } else {
                            let payload = { subject: username};
                            let token = jwt.sign(payload, 'secret');
                            res.send({status:1, data:req.body, token: token});
                        }
                    }
                );
            } else {
                console.log('BAD REGISTER');
                res.send({status: 0, error: "Username already used"});
            }
        })
    } catch (error) {
        res.send({status: 0, error: error});
    }

})


module.exports = router

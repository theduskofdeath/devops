//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('./server');
let should = chai.should();

describe('/POST Register', () => {
    it('it should send the register info', (done) => {
        chai.request(server)
            .post('/api/register')
            .send({email: 'nicolas.test@gmail.com', username: 'nicotest', password: 'test123'})
            .end((err, res) => {
                res.body.should.have.property('status').eql(1);
                done();
            });
    })
})

describe('/POST Register Username Already used', () => {
    it('it should not register the info', (done) => {
        chai.request(server)
            .post('/api/register')
            .send({email: 'nicolas.test@gmail.com', username: 'nicotest', password: 'test123'})
            .end((err, res) => {
                res.body.should.have.property('status').eql(0);
                res.body.should.have.property('error');
                done();
            });
    })
})

describe('/POST Login', () => {
    it('it should login by sending a good status (1)', (done) => {
        chai.request(server)
            .post('/api/login')
            .send({username: 'nicotest', password: 'test123'})
            .end((err, res) => {
                res.body.should.have.property('status').eql(1);
                res.body.should.have.property('token');
                done();
            });
    })
})

describe('/POST Wrong login', () => {
    it('it should not login and send a bad status (0)', (done) => {
        chai.request(server)
            .post('/api/login')
            .send({username: 'nicotest', password: 'test1234'})
            .end((err, res) => {
                res.body.should.have.property('status').eql(0);
                res.body.should.have.property('error');
                done();
            });
    })
})

describe('/GET PROFILE DUMP', () => {

    it('it should get the profile info', (done) => {
        chai.request(server)
            .post('/api/login')
            .send({username: 'nicotest', password: 'test123'})
            .end((err, res) => {
                res.body.should.have.property('token');
                const token = res.body.token;
                chai.request(server)
                    .get('/api/profile')
                    .set({"Authorization": `Bearer ${token}`})
                    .end((err, res) => {
                        res.body.should.have.property('status').eql(1);
                        res.body.should.have.property('data');
                    })
                done();
            });
    })
})

chai.use(chaiHttp);
